import Point from "./point.js"
const svgContainer = /** @type {SVGSVGElement}*/ (document.getElementById("patternContainer"))
const svgPoint1 = svgContainer.createSVGPoint()
const NS = "http://www.w3.org/2000/svg"

/**
 * @param {String} pathString 
 * @returns {Point[][]}
 */
const getPathData = (pathString) => {
	var pathStringList = pathString.replace(/,/g, " ").split(" ")
	var pathData = []
	let currentPathCount = -1
	//just to supress undefined warning
	let lastPoint = new Point(0, 0)
	for (let i = 0; i < pathStringList.length; i++) {
		var char = pathStringList[i]
		if (char == "M") {
			currentPathCount++
			pathData.push([])
		} else if (char == "V") {
			lastPoint = new Point(lastPoint.x, parseFloat(pathStringList[i + 1]))
			pathData[currentPathCount].push(lastPoint)
			i++
		} else if (char == "H") {
			lastPoint = new Point(parseFloat(pathStringList[i + 1]), lastPoint.y)
			pathData[currentPathCount].push(lastPoint)
			i++
		} else if (!isNaN(parseFloat(pathStringList[i]))) {
			lastPoint = new Point(parseFloat(pathStringList[i]), parseFloat(pathStringList[i + 1]))
			pathData[currentPathCount].push(lastPoint)
			i++
		}
	}
	return pathData
}

/**
 * 
 * @param {Path} path 
 */
const compilePathData = (path) => {
	const data = path.pathData
	let pathString = ""
	for (let i = 0; i < data.length; i++) {
		const path = data[i]
		pathString += "M"
		for (const point of path) {
			pathString += ` ${point.x},${point.y} `
		}
		pathString += "Z"
	}
	path.element.setAttribute("d", pathString)
}

/**
 * 
 * @param {Point} point 
 */
const getTransformedPoint = (point) => {
	svgPoint1.x = point.x
	svgPoint1.y = point.y
	const matrix = svgContainer.getCTM().inverse()
	return svgPoint1.matrixTransform(matrix)
}

const getTransformedDifference = (point1, point2) => {
	const transformedPoint1 = getTransformedPoint(point1)
	const transformedPoint2 = getTransformedPoint(point2)
	return {
		x: transformedPoint1.x - transformedPoint2.x,
		y: transformedPoint1.y - transformedPoint2.y
	}
}

export default class Path {
	constructor(pathElement) {
		this.element = pathElement
		this.pathData = getPathData(pathElement.getAttribute("d"))
		compilePathData(this)
	}

	translate(currentPoint, previousPoint) {
		const diff = getTransformedDifference(currentPoint, previousPoint)
		this.pathData.map(function(path) { path.map(function(point) { point.add(diff.x, diff.y) }) })
		compilePathData(this)
	}

	showNodesForEditing() {
		for (const path of this.pathData) {
			for (const point of path) {
				var node = document.createElementNS(NS, "circle")
				node.classList.add("helper", "editorNode")
				node.setAttribute("cx", point.x + "")
				node.setAttribute("cy", point.y + "")
				node.setAttribute("r", 1 + "")
				node.setAttribute("index", this.pathData.indexOf(path) + "," + path.indexOf(point))
				svgContainer.appendChild(node)
			}
		}
	}

	translateEditNode(node, currentPoint, previousPoint) {
		const diff = getTransformedDifference(currentPoint, previousPoint)
		const indices = node.getAttribute("index").split(",")
		const pathPoint = this.pathData[parseInt(indices[0])][parseInt(indices[1])]
		pathPoint.add(diff.x, diff.y)
		node.setAttribute("cx", pathPoint.x +"")
		node.setAttribute("cy", pathPoint.y +"")
		compilePathData(this)
	}
}