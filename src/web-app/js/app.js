import { Load } from "./loader.js"
import { CenterDrawing } from "./view.js"
import { SetBindings } from "./events.js"

const start = () => {
	CenterDrawing()
	SetBindings()
}

window.onload = () => {
	Load("../../assets/images/drawing.svg", start)
}