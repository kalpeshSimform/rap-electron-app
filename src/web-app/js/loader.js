import Path from "./path.js"

const NS = "http://www.w3.org/2000/svg"

/**@type {AppTypes.PathCollection}*/
let paths = {}

const loadSVG = (source, callback) => {
	paths = {}
	const svgObject = document.createElement("object")
	svgObject.setAttribute("type", "image/svg+xml")
	svgObject.setAttribute("data", source)
	document.body.appendChild(svgObject)
	svgObject.onload = () => {
		const groupElement = svgObject.getSVGDocument().getElementsByTagName("g")[0]
		callback(groupElement)
		svgObject.parentElement.removeChild(svgObject)
	}
}

/**
 * @param {SVGGElement} groupElement 
 */
const insertSVG = (groupElement) => {
	//first clean any existing svg
	const patternContainer = document.getElementById("patternContainer")
	patternContainer.innerHTML = ""

	const parentLayer = document.createElementNS(NS, "g")
	patternContainer.appendChild(parentLayer)

	insertPaths(parentLayer, groupElement.children)
}

/**
 * @param {SVGGElement} svgGElement 
 * @param {SVGPathElement[]} svgElements 
 */
const insertPaths = (svgGElement, svgElements) => {
	for (const svgElement of svgElements) {
		if (svgElement.tagName == "path")
			insertPath(svgGElement, svgElement)
		else if (svgElement.tagName == "g") {
			const newGElement = document.createElementNS(NS, "g")
			svgGElement.appendChild(newGElement)
			insertPaths(newGElement, svgElement.children)
		}
	}
}

/**
 * 
 * @param {SVGGElement} gElement 
 * @param {SVGPathElement} pathElement 
 */
const insertPath = (gElement, pathElement) => {
	const newPathElement = document.createElementNS(NS, "path")
	if (pathElement.id)
		newPathElement.id = pathElement.id
	else
		newPathElement.id = "path_" + Math.round(Math.random() * 100000000)
	newPathElement.setAttribute("d", pathElement.getAttribute("d"))
	gElement.appendChild(newPathElement)
	paths[newPathElement.id] = new Path(newPathElement)
}

export function Load(svgFile, callback) {
	loadSVG(svgFile, (gElement) => {
		insertSVG(gElement)
		callback && callback()
	})
}

export function GetPaths() {
	return paths
}