import Point from "./point.js"
import { TranslatePage, ZoomPage } from "./view.js"
import { GetPaths } from "./loader.js"


const ACTIONS = {
	NONE: 0,
	TRANSLATE_PAGE: 1,
	ZOOM: 2,
	SELECT: 3,
	PATH_EDIT: 4,
	PATH_MOVE: 5
}

const editingArea = document.getElementById("editingArea")
const svgContainer = document.getElementById("patternContainer")
let currentAction = ACTIONS.NONE
/** @type {Path} */
let currentPath, currentNode
const currentPoint = new Point(),
	previousPoint = new Point()

// const indicator = document.createElement("div")
// indicator.className = "indicator"
// document.body.appendChild(indicator)

export function SetBindings() {
	document.addEventListener("keydown", onContainerKeyDown)
	document.addEventListener("keyup", onDocumentKeyUp)
	editingArea.addEventListener("mousemove", onEditingAreaMouseMove)
	document.addEventListener("mouseup", onDocumentMouseUp)
	editingArea.addEventListener("wheel", onEditingAreaMouseWheel)
	for (const path of svgContainer.firstElementChild.children) {
		path.addEventListener("mousedown", onPathMouseDown)
	}
	for (const path of svgContainer.getElementsByTagName("path")) {
		path.addEventListener("click", onPathClick)
	}
}

const onContainerKeyDown = (event) => {
	if (event.keyCode == 32) {
		currentAction = ACTIONS.TRANSLATE_PAGE
	}
}

const onDocumentKeyUp = (event) => {
	if (event.keyCode == 32)
		currentAction = ACTIONS.NONE
}

const onEditingAreaMouseMove = (event) => {
	currentPoint.set(event.clientX, event.clientY)
	switch (currentAction) {
		case ACTIONS.TRANSLATE_PAGE:
			TranslatePage(currentPoint.x - previousPoint.x, currentPoint.y - previousPoint.y)
			break
		case ACTIONS.PATH_MOVE:
			currentPath.translate(currentPoint, previousPoint)
			break
		case ACTIONS.PATH_EDIT:
			currentPath.translateEditNode(currentNode, currentPoint, previousPoint)
	}

	previousPoint.set(currentPoint)
}

const onEditingAreaMouseWheel = (event) => {
	event.preventDefault()
	// indicator.style.left = event.clientX + "px"
	// indicator.style.top = event.clientY + "px"
	ZoomPage(-event.deltaY * 0.001, event.clientX, event.clientY)
}

const onDocumentMouseUp = (event) => {
	if (currentAction == ACTIONS.NONE)
		cleanEditorNodes()
	currentAction = ACTIONS.NONE
}

const onPathMouseDown = (event) => {
	currentAction = ACTIONS.PATH_MOVE
	currentPath = GetPaths()[event.target.id]
}

const cleanEditorNodes = () => {
	var existingNodes = document.getElementsByClassName("editorNode")
	while (existingNodes.length) {
		existingNodes[0].parentElement.removeChild(existingNodes[0])
	}
}

const onPathClick = (event) => {
	cleanEditorNodes()
	currentPath = GetPaths()[event.target.id]
	currentPath.showNodesForEditing()
	const editorNodes = document.getElementsByClassName("editorNode")
	for (const editorNode of editorNodes) {
		editorNode.addEventListener("mousedown", function(event) {
			currentNode = event.target
			currentAction = ACTIONS.PATH_EDIT
		})
	}
}