import Point from "./point.js"

const container = document.getElementById("patternContainer")
const editingArea = document.getElementById("editingArea")
const viewState = {
	zoom: 1,
	translate: new Point()
}
const zoomLimits = [0.1, 5]
const zoomHelperPoint1 = new Point()
const zoomHelperPoint2 = new Point()

const halfPoint = new Point(editingArea.clientWidth / 2, editingArea.clientHeight / 2)


export function CenterDrawing() {
	const bounds = container.firstElementChild.getBBox()
	container.setAttribute("viewBox", `${bounds.x} ${bounds.y} ${bounds.width} ${bounds.height}`)
}

function writeTransform() {
	container.setAttribute("transform",
		`translate(${viewState.translate.x} ${viewState.translate.y})scale(${viewState.zoom})`)
}

export function TranslatePage(x, y) {
	viewState.translate.add(x, y)
	writeTransform()
}

export function ZoomPage(increment, x, y) {
	const prevZoom = viewState.zoom
	viewState.zoom += increment
	viewState.zoom = Math.min(Math.max(viewState.zoom, zoomLimits[0]), zoomLimits[1])
	zoomHelperPoint1.set(x, y).subtract(halfPoint)
	zoomHelperPoint2.set(viewState.translate).subtract(zoomHelperPoint1)
		.scale(viewState.zoom / prevZoom).add(zoomHelperPoint1)
	viewState.translate.set(zoomHelperPoint2)
	writeTransform()
}