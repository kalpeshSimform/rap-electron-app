const {app, BrowserWindow, Menu} = require("electron")
let menuTemplate = require("./menu.json")

app.on("ready", startApp)

function startApp() {
	createWindow()
	addMenu()
}

function createWindow() {
	const mainWindow = new BrowserWindow({
		show: false,
		webPreferences: {
			//nodeIntegration: true,
			// preload: "./preload.js"
		}
	})
	mainWindow.maximize()
	mainWindow.loadFile("./src/web-app/window.html")
	mainWindow.show()
	mainWindow.webContents.openDevTools()
}

function addMenu() {
	Menu.setApplicationMenu(Menu.buildFromTemplate(menuTemplate))
}